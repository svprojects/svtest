#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <fcntl.h>
#include <set>
#include <algorithm>

int set_nonblock(int fd) {
	int flags;
#if defined(O_NONBLOCK)
	if(-1 == (flags = fcntl(fd, F_GETFL, 0)))
		flags = 0;
	return fcntl(fd, F_SETFL, flags | O_NONBLOCK);
#else
	flags = 1;
	return ioctl(fd, FIONBIO, &flags);
#endif
}

int main(int argc, char** argv) {
	int master = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	std::set<int> slave_sockets;
	
	struct sockaddr_in socka;
	socka.sin_family = AF_INET;
	socka.sin_port = htons(12345);
	socka.sin_addr.s_addr = htonl(INADDR_ANY);
	bind(master, (struct sockaddr *)(&socka), sizeof(socka));

	set_nonblock(master);
	
	listen(master, SOMAXCONN);
	
	while(true) {
		fd_set set;
		FD_ZERO(&set);
		FD_SET(master, &set);
		for(auto iter = slave_sockets.begin(); iter != slave_sockets.end(); iter++) {
			FD_SET(*iter, &set);
		}
		int max = std::max(master, 
				*std::max_element(slave_sockets.begin(),
					slave_sockets.end()));
		select(max+1, &set, NULL, NULL, NULL);
		for(auto iter = slave_sockets.begin(); iter!=slave_sockets.end(); iter++) {
			if(FD_ISSET(*iter, &set)) {
				static char buffer[1024];
				int recv_size = recv(*iter, buffer, 1024, MSG_NOSIGNAL);
				if((recv_size == 0) && (errno != EAGAIN)) {
					shutdown(*iter, SHUT_RDWR);
					close(*iter);
					slave_sockets.erase(iter);
				}
				else if(recv_size != 0) {
					send(*iter, buffer, recv_size, MSG_NOSIGNAL);
				}
			}
		}
		if(FD_ISSET(master, &set)) {
			int slave_socket = accept(master, 0, 0);
			set_nonblock(slave_socket);
			slave_sockets.insert(slave_socket);
		}
	}

	return 0;
}
