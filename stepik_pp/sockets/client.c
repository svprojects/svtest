#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

int main(int argc, char** argv) {

	int soc = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	struct sockaddr_in socka;
	socka.sin_family = AF_INET;
	socka.sin_port = htons(12345);
	socka.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
	
	connect(soc, (struct sockaddr *)&socka, sizeof(socka));

	char buffer[] = "PING";
	
	send(soc, buffer, 4, MSG_NOSIGNAL);
	buffer[0]='\0';
	recv(soc, buffer, 4, MSG_NOSIGNAL);
	
	shutdown(soc, SHUT_RDWR);
	close(soc);
	printf("%s\n", buffer);
	return 0;
}
