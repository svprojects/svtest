#include <stdio.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

int main(int arg, char** argv) {
	int mastersocket = socket(
		AF_INET,
		SOCK_STREAM,
		IPPROTO_TCP);

	struct sockaddr_in socka;
	socka.sin_family = AF_INET;
	socka.sin_port = htons(12345);
	socka.sin_addr.s_addr = htonl(INADDR_ANY);
	bind(mastersocket, (struct sockaddr *) &socka, sizeof(socka));

	listen(mastersocket, SOMAXCONN);

	while(1) {
		int slave = accept(mastersocket, 0, 0);
		char buffer[5] = "test";
		recv(slave, buffer, 4, MSG_NOSIGNAL);
		send(slave, buffer, 4, MSG_NOSIGNAL);

		shutdown(slave, SHUT_RDWR);
		close(slave);
		printf("%s\n", buffer);
	}

	return 0;

}
