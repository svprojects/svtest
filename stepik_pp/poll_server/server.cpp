#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <fcntl.h>
#include <set>
#include <algorithm>
#include <poll.h>

#define POLL_SIZE 2048

int set_nonblock(int fd) {
	int flags;
#if defined(O_NONBLOCK)
	if(-1 == (flags = fcntl(fd, F_GETFL, 0)))
		flags = 0;
	return fcntl(fd, F_SETFL, flags | O_NONBLOCK);
#else
	flags = 1;
	return ioctl(fd, FIONBIO, &flags);
#endif
}

int main(int argc, char** argv) {
	int master = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	std::set<int> slave_sockets;
	
	struct sockaddr_in socka;
	socka.sin_family = AF_INET;
	socka.sin_port = htons(12345);
	socka.sin_addr.s_addr = htonl(INADDR_ANY);
	bind(master, (struct sockaddr *)(&socka), sizeof(socka));

	set_nonblock(master);
	
	listen(master, SOMAXCONN);

	struct pollfd set[POLL_SIZE];
	set[0].fd = master;
	set[0].events = POLLIN;
	
	while(true) {
		unsigned int index = 1;
		for(auto iter = slave_sockets.begin(); iter!=slave_sockets.end(); iter++) {
			set[index].fd = *iter;
			set[index].events = POLLIN;
			++index;
		}

		unsigned int size = 1 + slave_sockets.size();

		poll(set, size, -1);

		for(unsigned int i = 0; i < size; ++i) {
			if(set[i].revents & POLLIN) {
				if(i) {
					static char buffer[1024];
					int recvs = recv(set[i].fd, buffer, 1024, MSG_NOSIGNAL);
					if((recvs == 0) && (errno != EAGAIN)) {
						shutdown(set[i].fd, SHUT_RDWR);
						close(set[i].fd);
						slave_sockets.erase(i);
					}
					else if(recvs > 0) {
						//for(unsigned int j = 0; j < size; ++j) {
						//	send(set[j].fd, buffer, recvs, MSG_NOSIGNAL);
						//}
						send(set[i].fd, buffer, recvs, MSG_NOSIGNAL);
					}
				}
				else {
					int slave_socket = accept(master, 0, 0);
					set_nonblock(slave_socket);
					slave_sockets.insert(slave_socket);
				}
			}
		}
	}

	return 0;
}
