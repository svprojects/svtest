#include <iostream>
#include <map>

std::string derivative(std::string polynomial) {

    std::map<int, int> koef;

    int last_s=0;
    int last_deg=0;
    int last_mult = 0;
    int last_x = 0;

    std::string result;

    if(polynomial.size() == 0) return "0";
    for(size_t i = 1; i < polynomial.size(); ++i) {
        if(polynomial.at(i) == '*') last_mult = i;
        if(polynomial.at(i) == '^') last_deg = i+1;
        if(polynomial.at(i) == 'x') last_x = i;
        if((i+1 == polynomial.size()) || (polynomial.at(i) == '+') || (polynomial.at(i) == '-')) {

            if(last_x < last_s) continue;

            std::string k = "1";
            if(last_mult > last_s)
                      k = polynomial.substr(last_s, last_mult - last_s);
            else k = (polynomial.at(last_s) == '-') ? "-1" : "1";

            std::string deg = (last_deg > last_s) ?
                      polynomial.substr(last_deg, i+1-last_deg) : "1";

            int deg_int = std::stoi(deg);
            int k_int = std::stoi(k)*deg_int;
            --deg_int;

            if(koef.count(deg_int)) koef[deg_int]+=k_int;
            else koef.insert( {deg_int, k_int} );

            last_s = i;
        }
    }

    for(auto x = koef.rbegin(); x != koef.rend(); ++x) {
        if(x != koef.rbegin() && x->second > 0) result += "+";
        result += std::to_string(x->second);
        if(x->first<1) continue;
        result += "*x";
        if(x->first>1) {
            result += "^";
            result += std::to_string(x->first);
        }
    }


    return result;
}


int main()
{
    std::cout << "Hello World!" << std::endl;
    return 0;
}
