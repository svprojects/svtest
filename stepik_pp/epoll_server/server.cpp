#include <iostream>
#include <algorithm>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/epoll.h>

#include <unistd.h>
#include <netinet/in.h>
#include <fcntl.h>

#define MAX_EVENTS 10

int set_nonblock(int fd) {
	int flags;
#if defined(O_NONBLOCK)
	if(-1 == (flags = fcntl(fd, F_GETFL, 0)))
		flags = 0;
	return fcntl(fd, F_SETFL, flags | O_NONBLOCK);
#else
	flags = 1;
	return ioctl(fd, FIONBIO, &flags);
#endif
}

int main(int argc, char** argv) {
	int master = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	
	struct sockaddr_in socka;
	socka.sin_family = AF_INET;
	socka.sin_port = htons(12345);
	socka.sin_addr.s_addr = htonl(INADDR_ANY);
	bind(master, (struct sockaddr *)(&socka), sizeof(socka));

	set_nonblock(master);

	listen(master, SOMAXCONN );

	int epoll  = epoll_create1(0);
	struct epoll_event ev;
	ev.data.fd = master;
	ev.events = EPOLLIN;

	epoll_ctl(epoll, EPOLL_CTL_ADD, master, &ev);
	
	while(true) {
		struct epoll_event events[MAX_EVENTS];
		int n = epoll_wait(epoll, events, MAX_EVENTS, -1);

		for(unsigned int i = 0; i < n; ++i) {
			if(events[i].data.fd == master) {
				int slave = accept(master, 0, 0);
				set_nonblock(slave);
				struct epoll_event event;
				event.data.fd = slave;
				event.events = EPOLLIN;
				epoll_ctl(epoll, EPOLL_CTL_ADD, slave, &event);
			}
			else {
				static char buffer[1024];
				int res = recv(events[i].data.fd, buffer, 1024, MSG_NOSIGNAL);
				if((res == 0) && (errno != EAGAIN)) {
					shutdown(events[i].data.fd, SHUT_RDWR);
					close(events[i].data.fd);
				}
				else if(res > 0) {
					send(events[i].data.fd, buffer, res, MSG_NOSIGNAL);
				}
			}
		}
	}


	return 0;
}
