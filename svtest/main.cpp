#include <stdlib.h>
#include <iostream>
#include <bits/stdc++.h>

#define what(x) #x

using namespace std;

//Variadic templates
int sum() {return 0;}
template<typename T, typename... Args>
T sum(T a, Args ... args) { return a + sum(args ...); }

//Custom literal
long long operator "" _m(unsigned long long literal) { return literal; }
long double operator "" _cm(unsigned long long literal) { return literal / 100.0; }
long long operator "" _km(unsigned long long literal) { return literal * 1000; }

//Recursion
template<unsigned n>
struct Factorial { enum { value = (n * Factorial<n-1>::value) }; };
template<>
struct Factorial<0> { enum { value = 1}; };

template<int n, int r>
struct Choose { enum {value = (n * Choose<n-1, r-1>::value) / r}; };
template<int n>
struct Choose<n, 0> { enum {value = 1}; };

//Compile-time "If"
template <bool Condition, typename TrueResult, typename FalseResult>
class if_;
template <typename TrueResult, typename FalseResult>
struct if_<true, TrueResult, FalseResult> { typedef TrueResult result; };
template <typename TrueResult, typename FalseResult>
struct if_<false, TrueResult, FalseResult> { typedef FalseResult result; };

//additional funcitons
template<typename T, typename S>
auto print(T v, S msg) -> void {
    cout << msg << ": ";
    for(auto& i: v)
        cout << i << " ";
    cout << endl;
}

int main()
{
    cout << Factorial<0>::value << endl;
    cout << Factorial<1>::value << endl;
    cout << Factorial<2>::value << endl;
    cout << Factorial<3>::value << endl;
    cout << Factorial<4>::value << endl;

    typename if_<true, int, double>::result number(3);


    return 0;
}
