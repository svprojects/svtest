#include <iostream>
#include <bits/stdc++.h>

using namespace std;

//tuple
template<typename ... Args> struct svtuple {};

template<typename TFirst, typename ... TRest>
struct svtuple<TFirst, TRest...> : public svtuple<TRest ...>  {
    TFirst value;
};

//tuple indexes
template<size_t index, typename Ttuple> struct svelement;

template<typename TFirst, typename ... TRest>
struct svelement<0, svtuple<TFirst, TRest...>> {
    using Type_t = TFirst;
    using TypeTuple_t = svtuple<TFirst, TRest...>;
};

template<size_t index, typename TFirst, typename ... TRest>
struct svelement<index, svtuple<TFirst, TRest ...>> :
        public svelement<index - 1, svtuple<TRest ...>> {};


//funciton
template<size_t index, typename ... TTypes>
typename svelement<index, svtuple<TTypes...>>::Type_t&
get_svelement(svtuple<TTypes...>& tuple) {
    using TypeTuple_t = typename svelement<index, svtuple<TTypes...>>::TypeTuple_t;
    return static_cast<TypeTuple_t&>(tuple).value;
}

template <size_t, typename TTyple>
void fill_svtuple(TTyple&) {}

template <size_t index, typename TTuple, typename TFirst, typename ... TRest >
void fill_svtuple(TTuple& tuple, const TFirst& firts, const TRest& ... rest) {
    get_svelement<index>(tuple) = firts;
    fill_svtuple<index+1>(tuple, rest ...);
}

template<typename ... TTypes>
svtuple<TTypes ... > make_svtuple(TTypes ... args) {
    svtuple<TTypes ...> tuple;
    fill_svtuple<0>(tuple, args ...);
    return tuple;
}


int main()
{
    auto tuple = make_svtuple(1, 2, 3);
    auto v0 = get_svelement<0>(tuple);
    auto v1 = get_svelement<1>(tuple);
    auto v2 = get_svelement<2>(tuple);

    return 0;
}
