#include <stdlib.h>
#include <iostream>

using namespace std;

///structs for test
struct Test { void bar(){ cout << "bar()" << endl; } };
struct Test2 { void foo(){ cout << "bar()" << endl; } };

///sfinae struct function detector
template<typename T>
struct has_bar{
    static int detect(...);
    template<typename U> static decltype(declval<U>().bar()) detect(const U&);
    static constexpr bool value = is_same<void, decltype(detect(declval<T>()))>::value;
};

///enabled function via return type
//template<class T>
//typename enable_if<has_bar<T>::value, T>::type viewbar(T val) {
//    val.bar();
//    return val;
//}
//template<class T>
//typename enable_if<!(has_bar<T>::value), T>::type viewbar(T val) {
//    cout << "bar is over!" << endl;
//    return val;
//}

///enabled function via template parametr
//template<class T, class = typename enable_if<has_bar<T>::value, T>::type >
//void  viewbar(T val) { val.bar(); }
//template<class T, class = typename enable_if<!(has_bar<T>::value), T>::type >
//viewbar(T val) { cout << "bar is over!" << endl; }

///enabled function via default function argument
template<class T>
void viewbar(T val, typename enable_if<has_bar<T>::value, T>::type* = 0) { val.bar(); }
template<class T>
void viewbar(T, typename enable_if<!(has_bar<T>::value), T>::type* = 0) { cout << "bar is over!" << endl; }


///enable funciton via constexp if in c++17
//not yet

int main(int, char**)
{
    viewbar(Test());
    viewbar(Test2());
    return 0;
}
